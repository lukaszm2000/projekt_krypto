import os

import pandas as pd
from flask import Flask, render_template, url_for, redirect, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_alembic import Alembic
from flask_login import login_user, LoginManager, login_required, logout_user, current_user
from flask_bcrypt import Bcrypt
import secrets


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///krypto.db"
app.config['SECRET_KEY'] = "thisisasecretkey"

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
alembic = Alembic()
alembic.init_app(app)


from myapp.models import User, KryptoData
from myapp.forms import RegisterForm, LoginForm
from myapp.klasa import Krypto
from myapp.functions import increment_requests_counter, get_values


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.route('/test/<nazwa>/<od>/<do>')
def test(nazwa, od, do):
    api_key = request.values.get('api_key')
    if api_key and db.session.query(User).filter(User.api_key == api_key).first():
        zapytanie = db.session.query(KryptoData).filter(KryptoData.currency == nazwa).filter((KryptoData.day >= od) & (KryptoData.day <= do))
        wartosci = pd.read_sql(zapytanie.statement, db.session.bind).set_index('day').to_dict('index')
        resp = {"data": wartosci}
        increment_requests_counter(api_key)
    else:
        resp = {"status": "wrong api key"}
    return jsonify(resp)


@app.route('/analiza/')
def analiza():
    api_key = request.values.get('api_key')
    if api_key and db.session.query(User).filter(User.api_key == api_key).first():
        zapytanie = db.session.query(KryptoData)
        f = get_values(db, zapytanie, KryptoData)
        increment_requests_counter(api_key)
    else:
        f = {"status": "wrong api key"}
    return f


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if bcrypt.check_password_hash(user.password, form.password.data):
                login_user(user)
                return redirect(url_for('dashboard'))
    return render_template('login.html', form=form)


@app.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dashboard():
    uid = current_user.get_id()
    user = db.session.query(User).filter(User.id == uid).first()
    return render_template('dashboard.html', variable=user.api_key)


@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data)
        new_user = User(username=form.username.data, password=hashed_password, api_key=secrets.token_urlsafe(nbytes=16))
        db.session.add(new_user)
        db.session.commit()
        return redirect(url_for('login'))
    return render_template('register.html', form=form)


@app.route('/pobierz_dane/<secret_key>/')
def dane(secret_key):
    if secret_key == os.getenv("DATA_KEY"):
        zapis = Krypto(db, KryptoData)
        zapis.zapis_kryptowalut("lista_krypto.csv")
        zapis.get_data()
        return "Udalo sie"
    return "Wrong data key"


if __name__ == '__main__':
    app.run(debug=True)

