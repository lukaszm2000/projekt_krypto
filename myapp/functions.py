from app import User, db
from flask import request
import pandas as pd


def increment_requests_counter(api_key):
    user = db.session.query(User).filter(User.api_key == api_key).first()
    user.requests_count += 1
    db.session.add(user)
    db.session.commit()


def get_values(db, question, model):
    if od := request.values.get('od'):
        question = question.filter(model.day >= od)
    if do := request.values.get('do'):
        question = question.filter(model.day <= do)
    values = pd.read_sql(question.statement,db.session.bind)
    results = values.groupby(['currency'])['close_usd'].agg(['first', 'last'])
    differences = ((results['first'] - results['last']) / results['last']).round(2)
    max_value = differences.max()
    max_idx = differences.idxmax()
    min_value = differences.min()
    min_idx = differences.idxmin()
    best = question.filter(model.currency == max_idx)
    worst = question.filter(model.currency == min_idx)
    best_values = pd.read_sql(best.statement, db.session.bind)
    worst_values = pd.read_sql(worst.statement, db.session.bind)
    results_best_max = best_values.groupby(['currency'])['close_usd'].max()
    results_best_min = best_values.groupby(['currency'])['close_usd'].min()
    results_best_marketcap = best_values.groupby(['currency'])['market_cap_usd'].sum()
    results_worst_max = worst_values.groupby(['currency'])['close_usd'].max()
    results_worst_min = worst_values.groupby(['currency'])['close_usd'].min()
    results_worst_marketcap = worst_values.groupby(['currency'])['market_cap_usd'].sum()
    daty_max = values.groupby(['currency'])['day'].max()
    daty_min = values.groupby(['currency'])['day'].min()
    data_do = daty_max.max()
    data_od = daty_min.min()
    resp = {
        "data":
            {
                "all_krypto_and_differences": differences.to_dict(),
                "best": {
                    "krypto": max_idx,
                    "difference": max_value,
                    'max_value': results_best_max.values[0],
                    'min_value': results_best_min.values[0],
                    'sum_of_market_cap': results_best_marketcap.values[0]
                },

                "meta": {
                    "od": request.values.get('od') if od else data_od,
                    "do": request.values.get('do') if do else data_do
                },
                "worst": {
                    "krypto": min_idx,
                    "difference": min_value,
                    'max_value': results_worst_max.values[0],
                    'min_value': results_worst_min.values[0],
                    'sum_of_market_cap': results_worst_marketcap.values[0]
                }
            }
    }
    return resp
