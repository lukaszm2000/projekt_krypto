from app import db
from flask_login import UserMixin
from sqlalchemy import text


class KryptoData(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    day = db.Column(db.String)
    currency = db.Column(db.String)
    open_cny = db.Column(db.Float)
    open_usd = db.Column(db.Float)
    high_cny = db.Column(db.Float)
    high_usd = db.Column(db.Float)
    low_cny = db.Column(db.Float)
    low_usd = db.Column(db.Float)
    close_cny = db.Column(db.Float)
    close_usd = db.Column(db.Float)
    volume = db.Column(db.Float)
    market_cap_usd = db.Column(db.Float)
    __table_args__ = (db.UniqueConstraint('day', 'currency'),)


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    password = db.Column(db.String(60), nullable=False)
    api_key = db.Column(db.String(30), unique=True)
    requests_count = db.Column(db.Integer, server_default=text("0"), nullable=False)



