import os
import csv
import requests
from time import sleep
import pandas as pd
import sqlalchemy
import sqlite3


class Krypto:
    def __init__(self, db, datamodel):
        self.datamodel = datamodel
        self.db = db
        self.lista_krypto = []
        self.api_key = os.getenv("APIKEY")

    def zapis_kryptowalut(self, plik):
        with open(plik, "r") as f:
            reader = csv.reader(f)
            for wiersz in reader:
                self.lista_krypto.append(wiersz[0])

    def get_data(self):
        req_url = 'https://www.alphavantage.co/query?'
        new_data = []
        for krypto in self.lista_krypto[25:30]:
            payload = {'function': 'DIGITAL_CURRENCY_DAILY', 'symbol': krypto, 'market': 'CNY', 'apikey': self.api_key}
            r = requests.get(req_url, params=payload)
            data = r.json()['Time Series (Digital Currency Daily)']
            for k, v in data.items():
                zapis_dnia = {
                    "day": k,
                    "currency": krypto,
                    "open_cny": v["1a. open (CNY)"],
                    "open_usd": v["1b. open (USD)"],
                    "high_cny": v["2a. high (CNY)"],
                    "high_usd": v["2b. high (USD)"],
                    "low_cny": v["3a. low (CNY)"],
                    "low_usd": v["3b. low (USD)"],
                    "close_cny": v["4a. close (CNY)"],
                    "close_usd": v["4b. close (USD)"],
                    "volume": v["5. volume"],
                    "market_cap_usd": v["6. market cap (USD)"]
                }
                nowy_wpis_sql = self.datamodel(**zapis_dnia)
                self.db.session.add(nowy_wpis_sql)
                try:
                    self.db.session.commit()
                except (sqlalchemy.exc.IntegrityError, sqlite3.IntegrityError):
                    self.db.session.rollback()
                new_data.append(zapis_dnia)
            sleep(12)
        new_data = pd.json_normalize(new_data)
        new_data.to_csv("dane3.csv", index=False)
